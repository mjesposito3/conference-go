from email import headerregistry
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests, json


def get_pic(city, state):
    headers = {"authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}

    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_weather(city, state):
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": (city, state, "US"),
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    geo_response = requests.get(geo_url, params=params)
    geo_content = json.loads(geo_response.content)
    lat = geo_content[0]["lat"]
    long = geo_content[0]["lon"]
    url = "https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}"
    params = {
        "lat, lon": (lat, long),
        "appid": OPEN_WEATHER_API_KEY
        }
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        temp = 1.8*(content["temp"]-273)+32   #F = 1.8*(K-273) + 32
        weather_description = content["description"]
        return {"weather": {"temp": temp, "description": weather_description,}}
    except:
        return {"weather": None}



# Create the URL for the geocoding API with the city and state
# Make the request
# Parse the JSON response
# Get the latitude and longitude from the response

# Create the URL for the current weather API with the latitude
#   and longitude
# Make the request
# Parse the JSON response
# Get the main temperature and the weather's description and put
#   them in a dictionary
# Return the dictionary
